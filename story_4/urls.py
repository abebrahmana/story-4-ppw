"""story_4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from app1_story4 import views as usrview

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', usrview.index, name="index"),
    path('blog/', usrview.blog, name="blog"),
    path('about/', usrview.about, name="about"),
    path('passion/', usrview.passion, name="passion"),
    path('work/', usrview.work, name="work"),
    path('modal/', usrview.modal, name="modal"),
]
