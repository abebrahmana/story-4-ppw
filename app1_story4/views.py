from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def index(request):
    return render(request, "app1_story4/index.html")

def blog(request):
    return render(request, "app1_story4/blog.html")

def about(request):
    return render(request, "app1_story4/about.html")

def passion(request):
    return render(request, "app1_story4/passion.html")

def work(request):
    return render(request, "app1_story4/work.html")

def modal(request):
    return render(request, "app1_story4/modal.html")